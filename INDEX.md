# tail

View the last part of a file. Like UNIX "Tail".

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## TAIL.LSM

<table>
<tr><td>title</td><td>tail</td></tr>
<tr><td>version</td><td>7.02a</td></tr>
<tr><td>entered&nbsp;date</td><td>1997-10-02</td></tr>
<tr><td>description</td><td>View the last part of a file</td></tr>
<tr><td>keywords</td><td>tail, unix, file</td></tr>
<tr><td>author</td><td>M.Aitchison _AT_ phys.canterbury.ac.nz</td></tr>
<tr><td>maintained&nbsp;by</td><td>M.Aitchison _AT_ cyberxpress.co.nz</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Open Source</td></tr>
<tr><td>summary</td><td>View the last part of a file. Like UNIX "Tail".</td></tr>
</table>
